<?php
function tentukan_nilai($number)
{
    if ($number > 85) {
        echo "Sangat Baik";
    } elseif ($number > 70) {
        echo "Baik";
    } elseif ($number > 60) {
        echo "Cukup";
    } else {
        echo "Kurang";
    }
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
